Service Standard is a shader designed for worlds. It uses the CoreRP package, in the Unity Package Manager, to provide higher quality lighting than Standard and some extra on top of that. It supports all kinds of baked lightmapping, but does NOT render add pass (i.e. point/spot) lights.  It adds steep parallax mapping and normal map shadows. 

I'd really like for people to test it out and see if it's good enough for public release.
It requires "Core RP Library" from the Package Manager.