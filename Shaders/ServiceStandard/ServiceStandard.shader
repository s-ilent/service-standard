Shader "ServiceStandard" {
	Properties {
		[Header(Main Parameters)][Space(5)]
		// (0.4663, 0.4663, 0.4663, 1)
		_AlbedoColor ("Color",      Color)            = (1, 1, 1, 1)
		_Metallic    ("Metallic Scale",   Range(0.0, 1.0))  = 0.0
		_Smoothness  ("Smoothness Scale", Range(0.0, 1.0))  = 0.0
    	_EmitIntensity ("Emission Strength", Range(0, 10)) = 0
    	_OcclusionStrength ("Occlusion Strength", Range(0, 1)) = 0.0
		_Anisotropy	 ("Anisotropy Direction", Range(-1.0, 1.0)) = 0.0
		[Space(15)]
		_MainTex ("Color(RGB), Alpha(A)", 2D)    = "white" {}
		[NoScaleOffset] _MetallicGlossMap ("Metal(R), Occlude(G), Emission(B), Smooth(A)", 2D) = "white" {}
		[Toggle(_SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A)]_SmoothnessSourceAlbedoAlpha("Smoothness from Albedo Alpha", Float) = 0.0
		[Space(15)]
		[Toggle(_NORMALMAP)][Normal]_UseNormalMap ("@ Normal Map", Float) = 0
		[NoScaleOffset] _BumpMap ("Tangent Normal(RGB)", 2D) = "bump" {}
		[Space(15)]
	    [Toggle(_PARALLAXMAP)]_UseParallax("@ Parallax Map", Float) = 0
	    [NoScaleOffset] _ParallaxMap ("Parallax(G)", 2D) = "grey" {}
	    [Gamma]_ParallaxStrength ("Parallax Strength", Range(0, 0.5)) = 0.02
		[Space(15)]
	    [Toggle(_SUNDISK_NONE)]_UseNormalMapShadows("@ Normal Map Shadows", Float) = 0
	    _HeightScale("Height Scale", Range(0, 1)) = 0.2
		_ShadowHardness("Shadow Hardness", Range(0, 100)) = 50
		[Space(15)]
		[Toggle(_DETAIL_MULX2)]_UseDetail("@ Detail Maps", Float) = 0
		_DetailAlbedoMap ("Color(RGB)", 2D)    = "grey" {}
		[Space(15)]
		[Toggle(_SUNDISK_SIMPLE)]_UseDLMSpecular("@ Directional Lightmap Specular", Float) = 0

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 100

		HLSLINCLUDE
		// Do not divide by PI, to match Standard.
		#define _NOPIDIV
		// If defined, these maps are read.
		#define _COLMAP
		#define _METMAP
		#define _OCCMAP
		#define _SMTMAP
		ENDHLSL

		Pass {
			Name "FORWARD"
			Tags{ "LightMode" = "ForwardBase"}
			ZWrite On
			Blend One Zero
			BlendOp Add
			HLSLPROGRAM
			#pragma target 5.0
			#pragma multi_compile_instancing
			#pragma multi_compile_fwdbase
			#pragma instancing_options assumeuniformscaling
			#pragma multi_compile _ VERTEXLIGHT_ON
			#pragma shader_feature DIRECTIONAL
			#pragma shader_feature SHADOWS_SCREEN
			#pragma multi_compile _ LIGHTPROBE_SH DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ UNITY_USE_NATIVE_HDR UNITY_LIGHTMAP_RGBM_ENCODING UNITY_LIGHTMAP_DLDR_ENCODING
			#pragma shader_feature DYNAMICLIGHTMAP_ON
			#pragma shader_feature _NORMALMAP
			#pragma shader_feature _PARALLAXMAP
			#pragma shader_feature _DETAIL_MULX2
			#pragma shader_feature _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A

			#pragma shader_feature _SUNDISK_NONE
			#define _NORMALMAP_SHADOWS defined(_SUNDISK_NONE)

			#pragma shader_feature _TERRAIN_NORMAL_MAP
			#define _TRIPLANAR defined(_TERRAIN_NORMAL_MAP)

			#pragma shader_feature _SUNDISK_SIMPLE
			#define _DLM_SPECULAR defined(_SUNDISK_SIMPLE)

			#pragma vertex   ForwardVertex
			#pragma fragment ForwardFragment
			#define SERVICE_FORWARD
			#include "ServiceLib.hlsl"
			ENDHLSL
		}
		Pass {
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}
			ZWrite On
			ColorMask 0
			HLSLPROGRAM
			#pragma target 3.5
			#pragma multi_compile_instancing
			#pragma multi_compile_shadowcaster
			#pragma instancing_options assumeuniformscaling
			#pragma vertex   DepthOnlyVertex
			#pragma fragment DepthOnlyFragment
			#define SERVICE_SHADOWCASTER
			#include "ServiceLib.hlsl"
			ENDHLSL
		}
		Pass {
			Name "META"
			Tags{"LightMode" = "Meta"}
			Cull Off
			HLSLPROGRAM
			#pragma shader_feature EDITOR_VISUALIZATION
			#pragma vertex   MetaVertex
			#pragma fragment MetaFragment
			#define SERVICE_META
			#include "ServiceLib.hlsl"
			ENDHLSL
		}
	}
}