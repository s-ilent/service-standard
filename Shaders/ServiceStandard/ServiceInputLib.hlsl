#ifndef SERVICE_INPUT_INCLUDED
#define SERVICE_INPUT_INCLUDED

GeometryST GetGeometry(VertexOutput input, float2 uv)
{
    GeometryST output;
    output.posWS = float3(input.tangentToWorldAndPosWS[0].w, input.tangentToWorldAndPosWS[1].w, input.tangentToWorldAndPosWS[2].w);	
    float3 verTangentWS  = input.tangentToWorldAndPosWS[0].xyz;
    float3 verBinormalWS = input.tangentToWorldAndPosWS[1].xyz;
    output.verNormalWS   = normalize(input.tangentToWorldAndPosWS[2].xyz);

#ifdef _NORMALMAP
    half4  normalMap  = SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, uv);
    float3 normalMapTS;
    normalMapTS.xy    = normalMap.wy * 2.0 - 1.0;
    normalMapTS.z     = sqrt(1.0 - saturate(dot(normalMapTS.xy, normalMapTS.xy)));
    output.normalWS   = normalize(verTangentWS * normalMapTS.x + verBinormalWS * normalMapTS.y + output.verNormalWS * normalMapTS.z);
    output.tangentWS  = normalize(verTangentWS - dot(verTangentWS, output.normalWS) * output.normalWS);
    float3 newBB      = cross(output.normalWS, output.tangentWS);
    output.binormalWS = newBB * FastSign(dot(newBB, verBinormalWS));
#else
    output.normalWS   = output.verNormalWS;
    output.tangentWS  = normalize(verTangentWS);
    output.binormalWS = normalize(verBinormalWS);
#endif
    return output;
}

CameraST GetCamera(VertexOutput input, GeometryST geo)
{
    CameraST output;
#ifdef UNITY_SINGLE_PASS_STEREO
    input.posNDC.xy = TransformStereoScreenSpaceTex(input.posNDC.xy, input.posNDC.w);
#endif
    output.posWS       = _WorldSpaceCameraPos;
    output.dirWS       = normalize(input.viewDirWS);
    output.distanceWS  = LinearEyeDepth(geo.posWS, UNITY_MATRIX_V);
    output.pixelPosSCS = input.posNDC.xy / input.posNDC.w;
    return output;
}

LightST GetMainLight(CameraST cam)
{
    LightST output;
#if defined (DIRECTIONAL) || defined (DIRECTIONAL_COOKIE)
 #if defined(_NOPIDIV)
    output.color = _LightColor0.rgb *PI;
 #else
    output.color = _LightColor0.rgb;
 #endif
    half atten = 1.0;
 #if defined(SHADOWS_SCREEN)
	atten = SAMPLE_TEXTURE2D(_ShadowMapTexture, sampler_ShadowMapTexture, cam.pixelPosSCS).x;
 #endif
	output.atten = atten;
    output.dirWS = _WorldSpaceLightPos0.xyz;
#else
	output.color = 0;
	output.atten = 0;
	output.dirWS = float3(0,0,1);
#endif
	return output;
}

SubLightsGeometryST GetSubLightsGeometry(GeometryST geo)
{
    SubLightsGeometryST output;
    float4 toLightX = unity_4LightPosX0 - geo.posWS.x;
    float4 toLightY = unity_4LightPosY0 - geo.posWS.y;
    float4 toLightZ = unity_4LightPosZ0 - geo.posWS.z;
    float4 distanceSqr = 0.0;
    distanceSqr += toLightX * toLightX;
    distanceSqr += toLightY * toLightY;
    distanceSqr += toLightZ * toLightZ;
    output.lightVectorWS[0] = float3(toLightX.x, toLightY.x, toLightZ.x);
    output.lightVectorWS[1] = float3(toLightX.y, toLightY.y, toLightZ.y);
    output.lightVectorWS[2] = float3(toLightX.z, toLightY.z, toLightZ.z);
    output.lightVectorWS[3] = float3(toLightX.w, toLightY.w, toLightZ.w);
    output.distanceSqr[0] = distanceSqr.x;
    output.distanceSqr[1] = distanceSqr.y;
    output.distanceSqr[2] = distanceSqr.z;
    output.distanceSqr[3] = distanceSqr.w;
    output.lightAtten[0] = unity_4LightAtten0.x;
    output.lightAtten[1] = unity_4LightAtten0.y;
    output.lightAtten[2] = unity_4LightAtten0.z;
    output.lightAtten[3] = unity_4LightAtten0.w;
    return output;
}

LightST GetSubLight(uint index, SubLightsGeometryST subLightsGeo)
{
    LightST output;
#if defined(_NOPIDIV)
    output.color = unity_LightColor[index].xyz * PI;
#else
    output.color = unity_LightColor[index].xyz;
#endif

    UNITY_BRANCH if ((output.color.r + output.color.g + output.color.b) != 0.0)
    {
        float distanceSqr = max(subLightsGeo.distanceSqr[index], (PUNCTUAL_LIGHT_THRESHOLD * PUNCTUAL_LIGHT_THRESHOLD));
#if defined(_NOPIDIV)
		output.atten = 1.0 / (1.0 + distanceSqr * subLightsGeo.lightAtten[index]);
#else
        float invDistanceSqr   = 1.0 / distanceSqr;
        float lightAttenFactor = distanceSqr * subLightsGeo.lightAtten[index] * 0.04;
		lightAttenFactor      *= lightAttenFactor;
		lightAttenFactor       = saturate(1.0 - lightAttenFactor);
		lightAttenFactor      *= lightAttenFactor;
        output.atten = max(invDistanceSqr * lightAttenFactor, 0.0);
#endif
        output.dirWS = SafeNormalize(subLightsGeo.lightVectorWS[index]);
    }
    else
    {
        output.atten = 0.0;
        output.dirWS = float3(0,0,1);
    }
    return output;
}

MaterialST GetMaterial(float2 uv)
{
    MaterialST output;
    real4 colParams = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv);
    real4 matParams = SAMPLE_TEXTURE2D(_MetallicGlossMap, sampler_MetallicGlossMap, uv);
    float4 matColor   = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _AlbedoColor);
    float  metallic   = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _Metallic);
    float  anisotropy = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _Anisotropy);
    float  smoothness = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _Smoothness);
    float  emission   = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _EmitIntensity);
    float  occlusion  = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _OcclusionStrength);
#ifdef _DETAIL_MULX2
    float2 uvDetail  = TRANSFORM_TEX(uv.xy, _DetailAlbedoMap);
    real4 detParams = SAMPLE_TEXTURE2D(_DetailAlbedoMap, sampler_DetailAlbedoMap, uvDetail);
#endif

#ifdef _COLMAP 
    matColor   *= colParams;
#endif
#ifdef _DETAIL_MULX2
    // Double, in gamma space, in linear space
    matColor.rgb *= detParams.rgb * 4.59479380; 
#endif
#ifdef _METMAP
    metallic   *= matParams.x;
#endif
#ifdef _OCCMAP
    occlusion  =  lerp(1, matParams.y, occlusion);
#endif
	emission   *= matParams.z;
#ifdef _SMTMAP
#ifdef _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
    smoothness *= colParams.w;
#else
    smoothness *= matParams.w;
#endif
#endif

    float oneMinusReflectivity = (1.0 - metallic) * 0.96;
    output.albedoColor  = matColor.rgb * oneMinusReflectivity;
    output.reflectColor = lerp(half3(0.04, 0.04, 0.04), matColor.rgb, metallic);
    output.grazingTerm  = saturate(smoothness + (1.0 - oneMinusReflectivity));
#ifdef _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
    output.alpha        = 1.0;
#else
    output.alpha        = matColor.a;
#endif
    output.perceptualRoughness = PerceptualSmoothnessToPerceptualRoughness(smoothness);
    output.anisotropy = anisotropy;

    ConvertAnisotropyToRoughness(output.perceptualRoughness, anisotropy, output.anisoRoughness.x, output.anisoRoughness.y);
    output.anisoRoughness.x    = max(output.anisoRoughness.x, 0.0005);
    output.anisoRoughness.y    = max(output.anisoRoughness.y, 0.0005);
    output.surfaceReduction    = 1.0 / (output.perceptualRoughness * output.perceptualRoughness + 1.0);
    output.microOcclusion = occlusion;
    output.emitColor      = matColor.rgb * emission;

    output.testValue = 0;

    output.reflectOneForTest = lerp(0.04, 1.0, metallic);
    return output;
}

LitParamPerViewST GetLitParamPerView(GeometryST geo, CameraST cam, MaterialST mat)
{
    LitParamPerViewST output;
    output.specOcclusion = GetHorizonOcclusion(cam.dirWS, geo.normalWS, geo.verNormalWS, 0.8);
    output.NdotV = ClampNdotV(dot(geo.normalWS, cam.dirWS));
    output.envRefl_fv = F_Pow5(saturate(output.NdotV));

    float TdotV        = dot(geo.tangentWS,  cam.dirWS);
    float BdotV        = dot(geo.binormalWS, cam.dirWS);
    output.partLambdaV = GetSmithJointGGXAnisoPartLambdaV(TdotV, BdotV, output.NdotV, mat.anisoRoughness.x, mat.anisoRoughness.y);

	float3  anisotropyDirection = mat.anisotropy <= 0.0
		? geo.tangentWS
		: geo.binormalWS;
	float3  anisotropicTangent  = cross(anisotropyDirection, cam.dirWS);
	float3  anisotropicNormal   = cross(anisotropicTangent, anisotropyDirection);
	float   bendFactor          = abs(mat.anisotropy) * saturate(5.0 * mat.perceptualRoughness);
	float3  bentNormal          = normalize(lerp(geo.normalWS, anisotropicNormal, bendFactor));

    output.reflViewWS = reflect(-cam.dirWS, bentNormal);
    return output;
}

LitParamPerLightST GetLitByTheLight(GeometryST geo, CameraST cam, MaterialST mat, LitParamPerViewST lip, LightST theLight)
{
    LitParamPerLightST output = (LitParamPerLightST)0;
    float NdotL = dot(geo.normalWS, theLight.dirWS);
    UNITY_BRANCH if (NdotL > 0.0)
    {
        float3 halfDir = SafeNormalize(theLight.dirWS + cam.dirWS);
        float LdotV = dot(theLight.dirWS, cam.dirWS);
        float NdotH = dot(geo.normalWS,   halfDir);
        float LdotH = dot(theLight.dirWS, halfDir);
        float TdotL = dot(geo.tangentWS,  theLight.dirWS);
        float BdotL = dot(geo.binormalWS, theLight.dirWS);
        float TdotH = dot(geo.tangentWS,  halfDir);
        float BdotH = dot(geo.binormalWS, halfDir);
        float spec_fv = F_Pow5(saturate(LdotH));
		float  occlusion    = ComputeMicroShadowing(mat.microOcclusion * 1.6 +0.2, NdotL, 1.0);
		float3 occlusionCol = GTAOMultiBounce(occlusion, mat.albedoColor);

        float  specTermD     = D_GGXAniso(TdotH, BdotH, NdotH, mat.anisoRoughness.x, mat.anisoRoughness.y);
        float  specTermG     = V_SmithJointGGXAniso(0, 0, lip.NdotV, TdotL, BdotL, NdotL, mat.anisoRoughness.x, mat.anisoRoughness.y, lip.partLambdaV);
        float3 specTermF     = mat.reflectColor + (1 - mat.reflectColor) * spec_fv;
		output.specularColor = (specTermD * specTermG * saturate(NdotL) * theLight.atten * occlusion * lip.specOcclusion) * specTermF * theLight.color;

        float  diffuseTerm   = DisneyDiffuse(lip.NdotV, NdotL, LdotV, mat.perceptualRoughness);
        output.diffuseColor  = (diffuseTerm * saturate(NdotL) * theLight.atten * occlusionCol) * theLight.color;

        output.testValue = 0;
    }
	return output;
}

real4 SampleLightmapBicubic(float2 uv)
{
    #ifdef SHADER_API_D3D11
        float width, height;
        unity_Lightmap.GetDimensions(width, height);

        float4 unity_Lightmap_TexelSize = float4(width, height, 1.0/width, 1.0/height);

        return SampleTexture2DBicubicFilter(TEXTURE2D_PARAM(unity_Lightmap, samplerunity_Lightmap),
            uv, unity_Lightmap_TexelSize);
    #else
        return SAMPLE_TEXTURE2D(unity_Lightmap, samplerunity_Lightmap, uv);
    #endif
}

real4 SampleDynamicLightmapBicubic(float2 uv)
{
    #ifdef SHADER_API_D3D11
        float width, height;
        unity_DynamicLightmap.GetDimensions(width, height);

        float4 unity_DynamicLightmap_TexelSize = float4(width, height, 1.0/width, 1.0/height);

        return SampleTexture2DBicubicFilter(TEXTURE2D_PARAM(unity_DynamicLightmap, samplerunity_DynamicLightmap),
            uv, unity_DynamicLightmap_TexelSize);
    #else
        return SAMPLE_TEXTURE2D(unity_DynamicLightmap, samplerunity_DynamicLightmap, uv);
    #endif
}

LitParamPerEnvironmentST GetLitByEnvironment(VertexOutput input, GeometryST geo, CameraST cam, MaterialST mat, LitParamPerViewST lip)
{
    LitParamPerEnvironmentST output;
	float  occlusion    = ComputeMicroShadowing(mat.microOcclusion * 0.8 + 0.3, lip.NdotV, 1.0);
	float3 occlusionCol = GTAOMultiBounce( saturate(mat.microOcclusion * 1.2), mat.albedoColor);

    float3 derivedIrradiance = 0;

#if defined(LIGHTPROBE_SH)
    output.diffuseColor      = max( SHEvalLinearL0L1(geo.normalWS, unity_SHAr, unity_SHAg, unity_SHAb)+ input.ambientOrLightmapUV.rgb, 0.0);
#elif defined(LIGHTMAP_ON)
	half4 decodeInstructions = half4(LIGHTMAP_HDR_MULTIPLIER, LIGHTMAP_HDR_EXPONENT, 0.0h, 0.0h);
	{
#if 1
		float4 encodedIlluminance = SampleLightmapBicubic(input.ambientOrLightmapUV.xy);
#else
        float4 encodedIlluminance = SAMPLE_TEXTURE2D(unity_Lightmap,    samplerunity_Lightmap, input.ambientOrLightmapUV.xy);
#endif
		float3 illuminance        = DecodeLightmap(encodedIlluminance, decodeInstructions);
		#if defined(DIRLIGHTMAP_COMBINED) 
		 float4 direction          = SAMPLE_TEXTURE2D(unity_LightmapInd, samplerunity_Lightmap, input.ambientOrLightmapUV.xy);
		 float  halfLambert        = dot(geo.normalWS, direction.xyz - 0.5) + 0.5;
		 output.diffuseColor       = illuminance * halfLambert / max(1e-4, direction.w);
         #if _DLM_SPECULAR
          half3 halfDir            = SafeNormalize(normalize(direction.xyz) + cam.dirWS);
          half  NdotH              = dot(geo.normalWS,   halfDir);
          half  capRoughness       = PerceptualRoughnessToRoughness(max(mat.perceptualRoughness, 0.3));
          derivedIrradiance        = D_GGX(NdotH, capRoughness) * illuminance;
         #endif
		#else
		 output.diffuseColor       = illuminance;
		#endif
	}
 #if defined(DYNAMICLIGHTMAP_ON)
	{
        float4 encodedIlluminance = SampleDynamicLightmapBicubic(input.ambientOrLightmapUV.zw);
		//float4 encodedIlluminance = SAMPLE_TEXTURE2D(unity_DynamicLightmap,		  samplerunity_DynamicLightmap, input.ambientOrLightmapUV.zw);
		float3 illuminance        = DecodeLightmap(encodedIlluminance, decodeInstructions);
		#if defined(DIRLIGHTMAP_COMBINED)
		 float4 direction          = SAMPLE_TEXTURE2D(unity_DynamicDirectionality, samplerunity_DynamicLightmap, input.ambientOrLightmapUV.zw);
		 float  halfLambert        = dot(geo.normalWS, direction.xyz - 0.5) + 0.5;
		 output.diffuseColor      += illuminance * halfLambert / max(1e-4, direction.w);
         #if _DLM_SPECULAR
          half3 halfDir            = SafeNormalize(normalize(direction.xyz) + cam.dirWS);
          half  NdotH              = dot(geo.normalWS,   halfDir);
          half  capRoughness       = PerceptualRoughnessToRoughness(max(mat.perceptualRoughness, 0.3));
          derivedIrradiance        = D_GGX(NdotH, capRoughness) * illuminance;
         #endif
		#else
		 output.diffuseColor      += illuminance;
		#endif
	}
 #endif
#else
    output.diffuseColor      = 0.0;
#endif
    output.diffuseColor *= occlusionCol;

    // Derived specular highlights from lightmap are tinted by the reflection colour and
    // are further attenuated by the perceptual roughness.
#if _DLM_SPECULAR
    derivedIrradiance *= (mat.perceptualRoughness);
#endif

#if defined(UNITY_SPECCUBE_BOX_PROJECTION)
    float3 reflViewWS = BoxProjectedCubemapDirection(lip.reflViewWS, geo.posWS, 
    	unity_SpecCube0_ProbePosition, unity_SpecCube0_BoxMin, unity_SpecCube0_BoxMax);
#else
    float3 reflViewWS = lip.reflViewWS;
#endif

    half  reflMipLevel      = PerceptualRoughnessToMipmapLevel(mat.perceptualRoughness);
    half4 encodedIrradiance = SAMPLE_TEXTURECUBE_LOD(unity_SpecCube0, samplerunity_SpecCube0, reflViewWS, reflMipLevel);
#if !defined(UNITY_USE_NATIVE_HDR)
    half3 irradiance = DecodeHDREnvironment(encodedIrradiance, unity_SpecCube0_HDR);
#else
    half3 irradiance = encodedIrradiance.rbg;
#endif

    float derivedOcclusion = 1.0;
#if defined(LIGHTMAP_ON)
    derivedOcclusion = saturate(length(output.diffuseColor)*3.0);
#endif
    
    // To avoid adding too much energy, dampen derived irradiance where we receive 
    // probe irradiance. 
    derivedIrradiance *= dot(saturate(derivedIrradiance - irradiance), 1.0/3.0);
    irradiance = (irradiance + derivedIrradiance);

    output.reflectColor = mat.microOcclusion * mat.surfaceReduction * irradiance * lerp(mat.reflectColor, mat.grazingTerm, lip.envRefl_fv) * derivedOcclusion;

    output.testValue = 0;

    return output;
}

#endif // SERVICE_INPUT_INCLUDED