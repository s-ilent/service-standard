#ifndef SERVICE_LIB_INCLUDED
#define SERVICE_LIB_INCLUDED
// ------------------------------------------------------------------------------------

// COMMON FOR ALL PASS
#include "ServiceCommon.hlsl"

// ------------------------------------------------------------------------------------
//
#ifdef SERVICE_FORWARD

#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/CommonMaterial.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/CommonLighting.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/BSDF.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/EntityLighting.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/ImageBasedLighting.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/SpaceTransforms.hlsl"

#include "ServiceFilteringLib.hlsl"

half4 _LightColor0;

UNITY_INSTANCING_BUFFER_START(PerInstance)
	UNITY_DEFINE_INSTANCED_PROP(float4, _AlbedoColor)
	UNITY_DEFINE_INSTANCED_PROP(float, _Metallic)
	UNITY_DEFINE_INSTANCED_PROP(float, _Anisotropy)
	UNITY_DEFINE_INSTANCED_PROP(float, _Smoothness)
	UNITY_DEFINE_INSTANCED_PROP(float, _EmitIntensity)

    UNITY_DEFINE_INSTANCED_PROP(float, _ParallaxStrength)
    UNITY_DEFINE_INSTANCED_PROP(float, _OcclusionStrength)

    UNITY_DEFINE_INSTANCED_PROP(float, _HeightScale)
    UNITY_DEFINE_INSTANCED_PROP(float, _ShadowHardness)
UNITY_INSTANCING_BUFFER_END(PerInstance)

TEXTURE2D_SHADOW(_ShadowMapTexture);	SAMPLER(sampler_ShadowMapTexture);
TEXTURECUBE(unity_SpecCube0);			SAMPLER(samplerunity_SpecCube0);
TEXTURE2D(unity_Lightmap);				SAMPLER(samplerunity_Lightmap);
TEXTURE2D(unity_LightmapInd);
TEXTURE2D(unity_DynamicLightmap);		SAMPLER(samplerunity_DynamicLightmap);
TEXTURE2D(unity_DynamicDirectionality);
TEXTURE2D(_MainTex);					SAMPLER(sampler_MainTex);
TEXTURE2D(_MetallicGlossMap);			SAMPLER(sampler_MetallicGlossMap);
TEXTURE2D(_BumpMap);					SAMPLER(sampler_BumpMap);

float4 _MainTex_ST;

TEXTURE2D(_ParallaxMap);                SAMPLER(sampler_ParallaxMap);
float4 _ParallaxMap_ST;

TEXTURE2D(_DetailAlbedoMap);                SAMPLER(sampler_DetailAlbedoMap);
float4 _DetailAlbedoMap_ST;

// ------------------------------------------------------------------
struct VertexInput
{
    float4 posOS	 : POSITION;
    float3 normalOS  : NORMAL;
    float4 tangentOS : TANGENT;
    float4 uv0		 : TEXCOORD0;
    float2 uvLM		 : TEXCOORD1;
    float2 uvDLM	 : TEXCOORD2;
    float4 vColor	 : COLOR;
	UNITY_VERTEX_INPUT_INSTANCE_ID
    UNITY_VERTEX_OUTPUT_STEREO
};
struct VertexOutput
{
    float4 posCS					 : SV_POSITION;
    float4 uv						 : TEXCOORD0;
    float4 tangentToWorldAndPosWS[3] : TEXCOORD1;
    float3 viewDirWS				 : TEXCOORD4;
    float4 posNDC					 : TEXCOORD5;
    // Fix for issue when lightmap UV is out of bounds at glancing angles:
    // Use centroid interpolation for this parameter.
    float4 ambientOrLightmapUV		 : TEXCOORD6_centroid;
    float3 viewDirTS                 : TEXCOORD7;
    float3 lightDirTS                : TEXCOORD8;
	UNITY_VERTEX_INPUT_INSTANCE_ID
    UNITY_VERTEX_OUTPUT_STEREO
};
struct GeometryST
{
    float3 posWS;
    float3 verNormalWS;
    float3 normalWS;
    float3 tangentWS;
    float3 binormalWS;
};
struct CameraST
{
    float3 posWS;
    float3 dirWS;
    float  distanceWS;
    float2 pixelPosSCS;
};
struct LightST
{
    float3 dirWS;
    float3 color;
    float  atten;
};
struct SubLightsGeometryST
{
    float3 lightVectorWS[4];
    float  distanceSqr[4];
    float  lightAtten[4];
};
struct MaterialST
{
    float3 albedoColor;
    float3 reflectColor;
    float  grazingTerm;
    float  alpha;
    float  perceptualRoughness;
    float  anisotropy;
    float2 anisoRoughness;
    float  surfaceReduction;
    float  microOcclusion;
    float3 emitColor;
    float3 testValue;
    float  reflectOneForTest;
};
struct LitParamPerViewST
{
    float  specOcclusion;
    float  NdotV;
    float  envRefl_fv;
    float3 reflViewWS;
    float  partLambdaV;
};
struct LitParamPerLightST
{
    float3 specularColor;
    float3 diffuseColor;
    float3 testValue;
};
struct LitParamPerEnvironmentST
{
    float3 reflectColor;
    float3 diffuseColor;
    float3 testValue;
};

float4 GetPosNDC(float4 posCS)
{
	float4 posNDC;
	float4 ndc = posCS * 0.5f;
	posNDC.xy  = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
	posNDC.zw  = posCS.zw;
	return posNDC;
}

float F_Pow5(float u)
{
	float x = 1.0 - u;
	float x2 = x * x;
	float x5 = x * x2 * x2;
	return x5;
}

float3 BoxProjectedCubemapDirection(float3 reflViewWS, float3 posWS, float4 cubemapCenter, float4 boxMin, float4 boxMax)
{
    UNITY_BRANCH if (cubemapCenter.w > 0.0)
    {
        float3 nrdir = normalize(reflViewWS);
		float3 rbmax    = (boxMax.xyz - posWS) / nrdir;
		float3 rbmin    = (boxMin.xyz - posWS) / nrdir;
		float3 rbminmax = (nrdir > 0.0f) ? rbmax : rbmin;
        float  fa       = min(min(rbminmax.x, rbminmax.y), rbminmax.z);
        posWS     -= cubemapCenter.xyz;
        reflViewWS = posWS + nrdir * fa;
    }
    return reflViewWS;
}

struct PerPixelHeightDisplacementParam
{
    float2 uv;
    float2 dX;
    float2 dY;
};

PerPixelHeightDisplacementParam InitPerPixelHeightDisplacementParam(float2 uv)
{
    PerPixelHeightDisplacementParam ppd;
 
    ppd.uv = uv;
    ppd.dX = ddx(uv);
    ppd.dY = ddy(uv);

    return ppd;
}

//#define USE_UNITY_PARALLAX

float ComputePerPixelHeightDisplacement(float2 offset, float lod, PerPixelHeightDisplacementParam ppdParam)
{
    float height = 1;
    float strength = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _ParallaxStrength);
    // Probably can use LOD to skip reading if too far
    height = 
        SAMPLE_TEXTURE2D_GRAD(_ParallaxMap, sampler_ParallaxMap, 
            ppdParam.uv + offset, ppdParam.dX, ppdParam.dY).g;

    height = clamp(height, 0, 0.9999);

    #if defined(USE_UNITY_PARALLAX)
    height = lerp(1, height, strength);
    #endif 

    return height;
}

#if defined(USE_UNITY_PARALLAX)
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/PerPixelDisplacement.hlsl"
#else
#include "ServiceParallaxLib.hlsl"
#endif

// R dither mask
float noiseR2(float2 pixel) {
    const float a1 = 0.75487766624669276;
    const float a2 = 0.569840290998;
    return frac(a1 * float(pixel.x) + a2 * float(pixel.y));
}

struct NormalMapShadowsParam
{
    float2 uv;
    float2 dX;
    float2 dY;
};

float3 SampleNormalMap (NormalMapShadowsParam nmsParam, float2 offset) {
    half4  normalMap  = SAMPLE_TEXTURE2D_GRAD(_BumpMap, sampler_BumpMap, 
        nmsParam.uv + offset, nmsParam.dX, nmsParam.dY);
    float3 normalMapTS;
    normalMapTS.xy    = normalMap.wy * 2.0 - 1.0;
    normalMapTS.z     = sqrt(1.0 - saturate(dot(normalMapTS.xy, normalMapTS.xy)));
    return normalMapTS;
}

#include "ServiceNormalShadowLib.hlsl"


// Return modified roughness based on provided variance 
// (get from GeometricNormalVariance + TextureNormalVariance)
float NormalFilteringRoughness(float roughness, float variance, float threshold)
{
    // Ref: Geometry into Shading - http://graphics.pixar.com/library/BumpRoughness/paper.pdf - equation (3)
    float squaredRoughness = saturate(roughness * roughness + min(2.0 * variance, threshold * threshold)); // threshold can be really low, square the value for easier control

    return sqrt(squaredRoughness);
}

// ------------------------------------------------------------------

#include "ServiceInputLib.hlsl"

// ------------------------------------------------------------------
VertexOutput ForwardVertex( VertexInput input)
{
    VertexOutput output;
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_TRANSFER_INSTANCE_ID(input, output);
    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

    float4 posWS = mul(UNITY_MATRIX_M, float4(input.posOS.xyz, 1.0));
    output.posCS = mul(UNITY_MATRIX_VP, posWS);

    float3   camPosWS = _WorldSpaceCameraPos;
    output.viewDirWS  = camPosWS - posWS.xyz;

    float3   normalWS   = normalize( mul( (float3x3) UNITY_MATRIX_M, input.normalOS));
    float4   tangentWS  = float4( normalize( mul( (float3x3) UNITY_MATRIX_M, input.tangentOS.xyz)), input.tangentOS.w);
    float    sign       = tangentWS.w * unity_WorldTransformParams.w;
	float3   binormalWS = cross( normalWS, tangentWS.xyz) * sign;

	float4 ndc       = output.posCS * 0.5f;
	output.posNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
	output.posNDC.zw = output.posCS.zw;

#ifdef LIGHTMAP_ON
	output.ambientOrLightmapUV.xy  = input.uvLM.xy  * unity_LightmapST.xy        + unity_LightmapST.zw;
 #ifdef DYNAMICLIGHTMAP_ON
	output.ambientOrLightmapUV.zw  = input.uvDLM.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
 #else
	output.ambientOrLightmapUV.zw  = 0;
 #endif
#elif LIGHTPROBE_SH
	output.ambientOrLightmapUV.rgb = SHEvalLinearL2(normalWS, unity_SHBr, unity_SHBg, unity_SHBb, unity_SHC);
	output.ambientOrLightmapUV.w   = 0;
#else
    output.ambientOrLightmapUV     = 0;
#endif

    output.uv.xy = input.uv0.xy;
    output.uv.zw = 0;
    output.tangentToWorldAndPosWS[0].xyz = tangentWS.xyz;
    output.tangentToWorldAndPosWS[1].xyz = binormalWS;
    output.tangentToWorldAndPosWS[2].xyz = normalWS;
    output.tangentToWorldAndPosWS[0].w = posWS.x;
    output.tangentToWorldAndPosWS[1].w = posWS.y;
    output.tangentToWorldAndPosWS[2].w = posWS.z;

    const real3x3 tangentToWorld = real3x3(tangentWS.xyz, binormalWS, normalWS);

    #ifdef _PARALLAXMAP
    output.viewDirTS = TransformWorldToTangent(output.viewDirWS, tangentToWorld);
    #endif

    #ifdef _NORMALMAP_SHADOWS
    float3 lightDirWS = normalize(_WorldSpaceLightPos0.xyz - posWS.xyz * _WorldSpaceLightPos0.w);
    output.lightDirTS = TransformWorldToTangent(lightDirWS, tangentToWorld);
    #endif

    UNITY_TRANSFER_VERTEX_OUTPUT_STEREO(input, output);
    
    return output;
}

float4 ForwardFragment( VertexOutput input ) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
    float2 uv  = TRANSFORM_TEX(input.uv.xy, _MainTex);

    float noise = noiseR2(input.posCS.xy);

    PerPixelHeightDisplacementParam ppd = InitPerPixelHeightDisplacementParam(uv);
 
    real height = 1;
 
    #ifdef _PARALLAXMAP
     // Todo: Find a nicer way to integrate this.
     #if defined(USE_UNITY_PARALLAX)
      uv += ParallaxOcclusionMapping(0, 0, 10, input.viewDirTS, ppd, height);
     #else
      float parallaxStrength = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _ParallaxStrength);
      input.viewDirTS = normalize(input.viewDirTS);
      input.viewDirTS.xy /= (input.viewDirTS.z + 0.42); 
      uv += ParallaxRaymarching(input.viewDirTS, ppd, parallaxStrength, height);
     #endif
    #endif // _PARALLAXMAP

    GeometryST        geo = GetGeometry(input, uv);
    CameraST          cam = GetCamera(input, geo);	
    MaterialST        mat = GetMaterial(uv);
    LitParamPerViewST lip = GetLitParamPerView(geo, cam, mat);

    LightST            sun    = GetMainLight(cam);

    // Todo: Find a nicer way to integrate this.
    #ifdef _NORMALMAP_SHADOWS
     float shadowHeight = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _HeightScale);
     float shadowHardness = UNITY_ACCESS_INSTANCED_PROP(PerInstance, _ShadowHardness);
     NormalMapShadowsParam nms = (NormalMapShadowsParam) ppd;
     nms.uv = uv;
     float nmShade = NormalMapShadows (input.lightDirTS, nms, noise, shadowHeight, shadowHardness);
     sun.atten = min(sun.atten, max(1-nmShade, 0));
    #endif

    // Filter roughness.
    float varianceGeom = GeometricNormalVariance(geo.verNormalWS, 0.5);
    float varianceNorm = TextureNormalVariance(length(geo.normalWS)); // not used
    float filteredRoughness = NormalFilteringRoughness(PerceptualRoughnessToRoughness(mat.perceptualRoughness),
        varianceGeom, 0.25);
    mat.perceptualRoughness = RoughnessToPerceptualRoughness(filteredRoughness);

    LitParamPerLightST litSun = GetLitByTheLight(geo, cam, mat, lip, sun);

    LitParamPerEnvironmentST litEnv = GetLitByEnvironment(input, geo, cam, mat, lip);

	LitParamPerLightST litSubLights;
	litSubLights.diffuseColor  = 0.0;
	litSubLights.specularColor = 0.0;
#ifdef LIGHTPROBE_SH
 #ifdef VERTEXLIGHT_ON
	SubLightsGeometryST subLightsGeo = GetSubLightsGeometry(geo);
	for (int i = 0; i < 3; i++) {
		LightST subLight = GetSubLight(i, subLightsGeo);
        UNITY_BRANCH if (subLight.atten != 0.0)
		{
            LitParamPerLightST litSubLight = GetLitByTheLight(geo, cam, mat, lip, subLight);
			litSubLights.diffuseColor  += litSubLight.diffuseColor;
			litSubLights.specularColor += litSubLight.specularColor;
		}
	}
 #endif
#endif

    float3 color = ( litSun.diffuseColor + litEnv.diffuseColor + litSubLights.diffuseColor ) * mat.albedoColor + litSun.specularColor + litEnv.reflectColor + litSubLights.specularColor + mat.emitColor;
	float  alpha = mat.alpha;

	return float4(color, alpha);
}

#endif //SERVICE_FORWARD
// ---------------------------------------------------------------------------
//
#ifdef SERVICE_SHADOWCASTER

struct VertexInput
{
    float4 posOS    : POSITION;
    float3 normalOS : NORMAL;
    UNITY_VERTEX_INPUT_INSTANCE_ID
    UNITY_VERTEX_OUTPUT_STEREO
};
struct VertexOutput
{
    float4 posCS : SV_POSITION;
    UNITY_VERTEX_OUTPUT_STEREO
};

// ------------------------------------------------------------------
VertexOutput DepthOnlyVertex(VertexInput input)
{
    VertexOutput output;
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

    float4 posWS = mul(UNITY_MATRIX_M, float4(input.posOS.xyz, 1.0));

    if (unity_LightShadowBias.z != 0.0)
    {
        float3 normalWS   = normalize(mul((float3x3) UNITY_MATRIX_M, input.normalOS));
        float3 lightDirWS = normalize(_WorldSpaceLightPos0.xyz - posWS.xyz * _WorldSpaceLightPos0.w);
        float  shadowCos  = dot(normalWS, lightDirWS);
        float  shadowSine = sqrt(1 - shadowCos * shadowCos);
        float  normalBias = unity_LightShadowBias.z * shadowSine;
        posWS.xyz        -= normalWS * normalBias;
    }

    output.posCS = mul(UNITY_MATRIX_VP, posWS);

    if (unity_LightShadowBias.y != 0.0)
    {
#ifdef UNITY_REVERSED_Z
		output.posCS.z += max(-1, min(unity_LightShadowBias.x / output.posCS.w, 0));
		output.posCS.z  = min(output.posCS.z, output.posCS.w * UNITY_NEAR_CLIP_VALUE);
#else
        output.posCS.z += saturate(unity_LightShadowBias.x / output.posCS.w);
        output.posCS.z  = max(output.posCS.z, output.posCS.w * UNITY_NEAR_CLIP_VALUE);
#endif
    }

    UNITY_TRANSFER_VERTEX_OUTPUT_STEREO(input, output);
    return output;
}

half4 DepthOnlyFragment(VertexOutput input) : SV_TARGET
{
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
    return 0;
}

#endif //SERVICE_SHADOWCASTER
// ---------------------------------------------------------------------------
//
#ifdef SERVICE_META

float4 _AlbedoColor;
float  _Metallic, _EmitIntensity;
float  unity_OneOverOutputBoost;
float  unity_MaxOutputValue;
float  unity_UseLinearSpace;

CBUFFER_START(UnityMetaPass)
	bool4 unity_MetaVertexControl;	 // x = use uv1 as raster position	// y = use uv2 as raster position
	bool4 unity_MetaFragmentControl; // x = return albedo				// y = return normal
CBUFFER_END

TEXTURE2D(_MainTex);            SAMPLER(sampler_MainTex);
TEXTURE2D(_MetallicGlossMap);	SAMPLER(sampler_MetallicGlossMap);

float4 _MainTex_ST;

// ------------------------------------------------------------------
struct VertexInput
{
    float4 posOS : POSITION;
    float2 uv0   : TEXCOORD0;
    float2 uvLM  : TEXCOORD1;
    float2 uvDLM : TEXCOORD2;
};
struct VertexOutput
{
    float4 posCS : SV_POSITION;
    float4 uv    : TEXCOORD0;
};
struct MaterialST
{
    float3 albedoColor;
    float3 emitColor;
};

// ------------------------------------------------------------------
MaterialST GetMaterial(float2 uv)
{
    MaterialST output;
    half4 colParams = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv);
    half4 matParams = SAMPLE_TEXTURE2D(_MetallicGlossMap, sampler_MetallicGlossMap, uv);
    float4 matColor = _AlbedoColor;
    float metallic = _Metallic;
    float emission = _EmitIntensity;
#ifdef _COLMAP
    matColor   *= colParams;
#endif
#ifdef _METMAP
    metallic   *= matParams.x;
#endif

#if !defined(EDITOR_VISUALIZATION)
	output.albedoColor = matColor.rgb *( 1.0 - metallic *0.5)  *( 0.5 + matColor.a *0.5) ;
#else
	output.albedoColor = matColor;
#endif

    output.emitColor = matColor.rgb * emission;
    return output;
}

// ------------------------------------------------------------------
VertexOutput MetaVertex(VertexInput input)
{
    VertexOutput output;

    float3 posTXS = input.posOS.xyz;
    if (unity_MetaVertexControl.x)
    {
        posTXS.xy = input.uvLM * unity_LightmapST.xy + unity_LightmapST.zw;
        posTXS.z  = posTXS.z > 0 ? REAL_MIN : 0.0f;
    }
    if (unity_MetaVertexControl.y)
    {
        posTXS.xy = input.uvDLM * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
        posTXS.z = posTXS.z > 0 ? REAL_MIN : 0.0f;
    }
    output.posCS = mul(UNITY_MATRIX_VP, float4(posTXS, 1.0));

    output.uv.xy = input.uv0.xy;
    output.uv.zw = 0;
    return output;
}

half4 MetaFragment(VertexOutput input) : SV_TARGET
{
    half4 color = 0;
    float2 uv = TRANSFORM_TEX(input.uv.xy, _MainTex);

    MaterialST mat = GetMaterial(uv);

    if (unity_MetaFragmentControl.x)
    {
        color = half4(mat.albedoColor, 1.0);    
        unity_OneOverOutputBoost = saturate(unity_OneOverOutputBoost);	// d3d9 shader compiler doesn't like NaNs and infinity.   
        color.rgb = clamp(PositivePow(color.rgb, unity_OneOverOutputBoost), 0, unity_MaxOutputValue);	// Apply Albedo Boost from LightmapSettings.
    }
    if (unity_MetaFragmentControl.y)
    {
        color = half4(mat.emitColor, 1.0);
    }
    return color;
}

#endif //SERVICE_META
// ---------------------------------------------------------------------------
// ------------------------------------------------------------------------------------
#endif //SERVICE_LIB_INCLUDED